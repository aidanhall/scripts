#!/bin/sh

PLAYLIST="$("$HOME/scripts/qutebrowser/get-playlist.sh" "Edit:" "/tmp/qutebrowser-video-playlist" )"
[ -z "$PLAYLIST" ] && exit

[ -f "$PLAYLIST" ] || PLAYLIST="$(xdg-user-dir DOCUMENTS)/playlists/$PLAYLIST"
# $TERMINAL "$EDITOR" "$PLAYLIST"
mimeopen -n "$PLAYLIST"
