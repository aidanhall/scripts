#!/bin/sh

# If 'default' is given as an argument, it loads from the default playlist, bypassing the dmenu prompt.

# mpv `cat /tmp/qutebrowser-video-playlist`
# Default check.
[ "$1" = "default" ] && PLAYLIST="/tmp/qutebrowser-video-playlist" || PLAYLIST="$("$HOME/scripts/qutebrowser/get-playlist.sh" "Play:" "/tmp/qutebrowser-video-playlist")"

if [ -n "$2" ] ; then
	QUALITY="$2"
else
	QUALITY="$(head -n 1 "$PLAYLIST" | youtube-dl -F -a - | dmenu -l 20 -p "Resolution:" | cut -d' ' -f1)"
fi

echo "Quality setting: $QUALITY"

[ -z "$PLAYLIST" ] && exit 1
# Prefix reinsertion.
[ -f "$PLAYLIST" ] || PLAYLIST="$(xdg-user-dir DOCUMENTS)/playlists/$PLAYLIST"

# Includes a basic regex to strip comments, denoted with %%.
sed 's/^https*:\/\///;s/^/https:\/\//;s/\s*%%.*$//;/^\s*$/d;s/^/"/;s/$/"/' "$PLAYLIST" | xargs mpv --player-operation-mode=pseudo-gui --ytdl-raw-options=format="$QUALITY/best,write-sub=,write-auto-sub=,externa-downloader=aria2c"
# $HOME/scripts/qutebrowser/clear-playlist.sh
