#!/bin/sh

PLAYLIST="$("$HOME/scripts/qutebrowser/get-playlist.sh" "Load:")"
[ -z "$PLAYLIST" ] && exit

cat "$(xdg-user-dir DOCUMENTS)/playlists/$PLAYLIST" >> /tmp/qutebrowser-video-playlist
notify-send "Loaded Playlist" "$PLAYLIST"
