#!/bin/sh

[ -z "$1" ] && notify-send "empty url" && exit 1

URL="$1"
echo "$URL" | sed 's/^https*:\/\///;s/^/https:\/\//;' >> /tmp/qutebrowser-video-playlist
notify-send -u low "Added to playlist:" "$URL"
