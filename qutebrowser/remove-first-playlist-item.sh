#!/bin/sh


REMAINING="$(tail -n "$(( `wc -l /tmp/qutebrowser-video-playlist | cut -d ' ' -f 1` - 1 ))" /tmp/qutebrowser-video-playlist)"
rm /tmp/qutebrowser-video-playlist

echo "$REMAINING" >> /tmp/qutebrowser-video-playlist
