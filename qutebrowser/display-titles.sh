#!/bin/sh

notify-send "Playlist:" "$(wget -qO- -i /tmp/qutebrowser-video-playlist | grep \<title\> | sed 's/.*<title>//g;s/<\/title>.*//g')"
