#!/bin/sh

PLAYLIST="$("$HOME/scripts/qutebrowser/get-playlist.sh" "Save:")"
[ -z "$PLAYLIST" ] && exit

cp /tmp/qutebrowser-video-playlist "$(xdg-user-dir DOCUMENTS)/playlists/$PLAYLIST"
notify-send "Saved Playlist" "$PLAYLIST"
