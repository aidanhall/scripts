#!/bin/sh

WINID="$(xdotool getactivewindow)"
SELECTED="$(cat /tmp/qutebrowser-video-playlist | dmenu -w "$WINID" -p Video: -l 10)"

[ -z "$SELECTED" ] && exit

#INVERTED="$( cat /tmp/qutebrowser-video-playlist | grep -v "$SELECTED" )"
#
#echo "$INVERTED" > /tmp/qutebrowser-video-playlist

mpv "$SELECTED"
