#!/bin/sh

# Usage: get-playlist.sh [prompt] [additional directories...]
# If [prompt] is absent or empty, "Playlist:" will be used as the prompt.
# The filenames in Documents/playlists are relative to that, the argument ones must be absolute.

PROMPT="Playlist:"

[ -n "$1" ] && PROMPT="$1" && shift

( [ -n "$1" ] && find "$@" -type f;
find "$(xdg-user-dir DOCUMENTS)/playlists" -type f -printf "%P\n" ) | dmenu -p "$PROMPT" -l 20
