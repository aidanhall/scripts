#!/bin/sh

PLAYLIST="$("$HOME/scripts/qutebrowser/get-playlist.sh" "Save:")"
[ -z "$PLAYLIST" ] && exit

curl -s `cat /tmp/qutebrowser-video-playlist` | grep \<title\> | sed 's/.*<title>//g;s/<\/title>.*//g' > /tmp/qutebrowser-video-titles

paste /tmp/qutebrowser-video-playlist /tmp/qutebrowser-video-titles | sed 's/\t/\t%%\t/' "$(xdg-user-dir DOCUMENTS)/playlists/$PLAYLIST"
notify-send "Saved Playlist" "$PLAYLIST"
