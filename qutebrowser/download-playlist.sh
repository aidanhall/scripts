#!/bin/sh

WINID="$(xdotool getactivewindow)"
FIRSTVID="$(head -n 1 /tmp/qutebrowser-video-playlist)"
QUALITY="$(youtube-dl -F "$FIRSTVID" | grep -v "^\[\|^format" | dmenu -p "Format:" -l 10 -w "$WINID" | cut -d ' ' -f 1)"

rm /tmp/qutebrowser-download-log

notify-send -u low "Downloading playlist contents."
youtube-dl --write-sub --sub-lang enUS --embed-subs -f "${QUALITY:-bestvideo+bestaudio}" --exec "mv {} '$(xdg-user-dir VIDEOS)/qutebrowser-videos' && /home/aidan/scripts/qutebrowser/remove-first-playlist-item.sh && notify-send -u low 'Finished downloading:' {}" -a /tmp/qutebrowser-video-playlist > /tmp/qutebrowser-download-log
