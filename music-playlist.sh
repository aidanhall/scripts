#!/bin/sh

# Downloads the link given, and automatically stores it in the correct directory in Music.
# Optimised for YouTube playlists for albums.
# Defaults to clipboard contents if no links are specified.

if [ "$1" = "-a" ] && [ -n "$2" ] ; then
    echo setting
    ARTIST="%($2)s"
    shift 2
elif [ "$1" = "-n" ] && [ -n "$2" ] ; then
    ARTIST="$2"
else
    ARTIST="%(creator)s"
fi
echo Artist: $ARTIST
if [ "$1" = "-f" ] && [ -n "$2" ] ; then
    FORMAT="$2"
    shift 2
else
    FORMAT="bestaudio"
fi
echo Format: $FORMAT

# while true; do
#     case "$1" in
# 	'-a')
# 	    ARTIST="$2"
# 	    shift 2
# 	    continue
# 	    ;;
# 	'-p')
# 	    VIDEOS="$2"
# 	    shift 2
# 	    continue
#     esac
# done
[ -z "$VIDEOS" ] && VIDEOS="$(xclip -o -selection clipboard)" || VIDEOS="$*"
[ -z "$VIDEOS" ] && exit 1

# --download-archive `xdg-user-dir MUSIC`/download-archive.txt \

yt-dlp -f "$FORMAT" --embed-thumbnail --add-metadata \
    -o "$(xdg-user-dir MUSIC)/$ARTIST/%(album)s/%(playlist_index)s - %(title)s.%(ext)s" $VIDEOS

notify-send "Finished Downloading" $VIDEOS
mpc update
