#!/bin/sh

# Move to a directory to store shows in.
cd $(xdg-user-dir VIDEOS)

# The search term to be sent to Crunchyroll's database.
printf "Name of show: "
read searchterm

# Use Crunchyroll's search engine to find the shows.
wget -O searchfile "https://www.crunchyroll.com/search?from=&q=$searchterm"

# Filter the result to only show the titles of shows and allow the user to select one.
selectedshow=$(grep clearfix searchfile | grep -v /library/ | grep href | sed 's/ *<a href="//g; s/" *class="clearfix">//g; s/\/en-gb\///g' | fzf)
[[ "$selectedshow" = "" ]] && exit 1

# Download the page for the show.
wget -O showfile "https://www.crunchyroll.com/$selectedshow"

# Filter that down to a list of urls for the episodes, in addition to an 'all' option which can be used to download all of the episodes.
urls="$(grep episode showfile | grep 'a h' | grep -v js-resume-play | sed 's/<a\ href="//g; s/https:\/\/www.crunchyroll.com//g; s/^ *//g; s/" .*//g; s/\/en-gb//g')
all"

rm searchfile showfile

echo "$urls"

chosen="https://www.crunchyroll.com/en-gb$(echo "$urls" | fzf)"

printf "Enter your password: "
read password

mpv --ytdl-raw-options=username=aidan.hall@outlook.com,password=$password "$chosen" & exit
