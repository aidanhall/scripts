#!/usr/bin/sed -f

# Definitely remove.
/google/Id;

# Things we want.
/searx/Ip;
/gitlab/Ip;
/youtube/Ip;

# Remove things we didn't ask for.
/^/d;
