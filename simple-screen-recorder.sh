#!/bin/sh
DELAY="${1:-0}"
sleep "$DELAY"
echo "Recording..."
FILENAME="$(xdg-user-dir VIDEOS)/recordings/screencap_$(date +%F_%H-%M-%S.mp4)"
# ( [ "$(hostname)" = x220 ] || pgrep droidcam > /dev/null ) &&
# [ -e /dev/video0 ] && CAMERAOPTION=" -f v4l2 -video_size 640x480 -i /dev/video0 -filter_complex overlay=W-w-20:H-h-40 " -c:v h264_nvenc -rc constqp 
ffmpeg -f alsa -i default -thread_queue_size 32  -f x11grab \
    $( xrandr -q | grep -m 1 "primary" | grep -Eo "[[:digit:]]*x[[:digit:]]*\+[[:digit:]]*\+[[:digit:]]*" | awk -F+ '{print "-video_size " $1 " -i :0.0+"$2"+"$3}' ) \
    $CAMERAOPTION \
    "$FILENAME"
echo "$FILENAME saved."
xclip -i -selection clipboard << EOF
$FILENAME
EOF
# ffmpeg -f alsa -i default -s 1920x1080 -f x11grab -i :0.0+1440,0 -i /dev/video0 -filter_complex "overlay=W-w-20:H/2" "out.mp4"
