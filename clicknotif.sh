#!/bin/sh

RES=$(notify-send --action=OPEN="Open screenshot file." -u low -i "$1" "Scrot" "$2 saved.")

[ OPEN = "$RES" ] && setsid mimeopen "$3"

