#!/bin/sh
DELAY="${1:-0}"
sleep "$DELAY"
FILENAME="$(xdg-user-dir VIDEOS)/recordings/screencap_$(date +%F_%H-%M-%S.mp4)"
# [ -e /dev/video0 ] && ( [ "$(hostname)" = x220 ] || pgrep droidcam > /dev/null ) &&  CAMERAOPTION=" -i /dev/video0 -filter_complex overlay=W-w-20:H-h-40 "
ffmpeg -f alsa -i default -f x11grab -i :0.0 $CAMERAOPTION "$FILENAME"
echo "$FILENAME saved."
