#!/bin/sh

# Depends on dmenu
pgrep -x dmenu && exit

mountable=$(lsblk -lp | grep "part *$" | awk '{print $1, "(" $4 ")"}' && simple-mtpfs --list-devices)
[ "$mountable" = "" ] && exit 1

chosen=$(echo "$mountable" | dmenu -l 20 -i -p "Mount:" | awk '{print $1}')
[ "$chosen" = "" ] && exit 1

case "$chosen" in
    /*) location=$(udisksctl mount -b "$chosen") && pgrep -x dunst && notify-send "$location"
	;;
    *:) simple-mtpfs --device "${chosen%:}" ~/phone && notify-send "Phone mounted."
	;;
esac
#echo ${location%.} | awk '{print $4}' | vifm -


