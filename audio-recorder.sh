#!/bin/sh
DELAY="${1:-0}"
sleep "$DELAY"
FILENAME="$(xdg-user-dir MUSIC)/recordings/audio_$(date +%F_%H-%M-%S.flac)"
ffmpeg -f alsa -i default "$FILENAME"
echo "$FILENAME saved."
