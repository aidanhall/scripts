#!/bin/gnuplot -c


f(x) = a*x + c
set fit limit 1e-6
fit f(x) ARG1 using @ARG2 : @ARG3 via a,c

if (ARGC > 3) {
    save fit ARG4
} else {
    save fit '-'
}

if (ARGC > 4) {
    set term ARG5
} else {
    set term qt
}
plot ARG1 u @ARG2 : @ARG3 title "Points" , f(x) title sprintf("%fx + %f", a, c)
# vim: ft=gnuplot
