#!/bin/sh

# GB keyboard and swap caps and escape for Vim quality-of-life improvements.
#setxkbmap -option ctrl:swapcaps_hyper gb
# Enable Scroll Lock? Potential for a key-binding layer?
[ -f ~/.config/Xmodmap ] && xmodmap ~/.config/Xmodmap
# Set shorter repeat delay. Bad because it de-emphasises efficient use of key-strokes? 
xset r rate 300

# Sticky keys.
# xkbset accessx sticky -twokey -latchlock
xkbset exp 1 =accessx =sticky =twokey -latchlock
