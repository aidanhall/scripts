#!/bin/sh

# The playlist must be a .txt file.
playlist=$1

playlistfolder=${playlist%.txt}
mkdir $playlistfolder
mv $playlist $playlistfolder
cd $playlistfolder
youtube-dl --write-sub --sub-lang enUS --restrict-filenames -a $playlist

rename .enUS.ass .ass find *.enUS.ass
