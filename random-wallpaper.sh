#!/bin/sh

# Wallpapers
WALLPAPER_DIRS="$HOME/Pictures/wallpapers /usr/share/backgrounds"

# Used with grep -vE, so separate them with '|'.
EXCLUDE_WALLPAPERS="vnc|VNC|archbtw|the-mouse|focal-ubuntukylin|firstgeneration|Symbolics-1"

# Allowed image formats based on extension.
FORMATS="jpg|jpeg|png"


echo "Setting random wallpaper."
# Sets a random wallpaper, from a list of wallpaper directories.
[ -z "$WALLPAPER_DIRS" ] && echo "Couldn't load wallpapers" && exit 1

WALLNAME="$(find $WALLPAPER_DIRS -type f -regextype posix-extended -regex ".*\.($FORMATS)"| grep -vE $EXCLUDE_WALLPAPERS | shuf -n 1 | tee "/tmp/$USER-wallpaper")"

feh --no-fehbg --bg-fill "$WALLNAME"

notify-send -u low -i "$WALLNAME" "Wallpaper:" "$WALLNAME"
