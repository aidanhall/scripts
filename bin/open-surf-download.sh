#!/bin/sh

SELECTION="$(find "$HOME/.surf/dlstatus/" -type f -printf "%P\n" | dmenu -p "OPEN:" -l 20)"

[ -n "$SELECTION" ] && setsid xdg-open "$HOME/Downloads/$SELECTION"
