#!/bin/sh

[ "$1" = "" ] && printf "Issue number: " && read -r issue_number || issue_number="$1"

curl "https://magpi.raspberrypi.org/issues/$issue_number/pdf" 

curl "$(curl https://magpi.raspberrypi.org/issues/$issue_number/pdf | grep "click here to get your free PDF" | sed 's/^.*href="//;s/">.*$//')" > /home/aidan/Documents/magpi/$issue_number.pdf
