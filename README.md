# Shell scripts.

This is, like most, if not all, of my projects, for personal use only.
However, please feel free to use any of these scripts if you wish.

The bin folder is intended for links to scripts that are added to the path.

The qutebrowser folder is for manipulating plain-text playlists.
I mainly use these in surf and through global bindings now.
