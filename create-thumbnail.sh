#!/bin/sh

LTEXT="${1:-"Top Left"}"
RTEXT="${2:-"Argletrough"}"
ICON="${3:-icon.png}"
OUTFILE="${4:-thumbnail.png}"
TEXTFNT="${5:-"monospace"}"
PSIZE="${6:-120}"
BACKGROUND="${7:-"plasma:black"}"

convert $ICON -resize x800 /tmp/icon-size.png
magick -size 1920x1080 $BACKGROUND /tmp/icon-size.png -gravity center -composite -family "$TEXTFNT" -pointsize $PSIZE -fill orange -gravity north -annotate 0 "$LTEXT" -fill cyan -gravity south -annotate 0 "$RTEXT" "$OUTFILE" && echo "Written to $OUTFILE."
