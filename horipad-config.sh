#!/bin/sh

#echo Unplug the controller.
#ls /dev/event*
#read
#echo Plug back in the controller.
#ls /dev/event*

xboxdrv --evdev /dev/input/event$1 --evdev-absmap ABS_HAT0X=dpad_x,ABS_HAT0Y=dpad_y --evdev-keymap BTN_NORTH=X,BTN_SOUTH=y,BTN_C=a,BTN_EAST=b,BTN_TL2=back,BTN_TR2=start,BTN_WEST=lb,BTN_TL=lt,BTN_Z=rb,BTN_TR=rt --mimic-xpad --silent &

