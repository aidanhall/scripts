#!/bin/sh
dmenu -p "MAN:" < /dev/null | xargs apropos | dmenu -l 20 -p "PAGE:" | awk '{print substr($2, 2, index($2, ")") - 2) " " $1}' | tee /tmp/pagename | xargs -n 1 test && xargs -a /tmp/pagename st -e man
