#!/bin/sh

activeid="$(xdotool getactivewindow)" 
([ -e /tmp/tabbed-surf.xid ] && pgrep -x tabbed) || (surf-open && sleep 0.5)

xdotool windowreparent "$activeid" "$(($(cat /tmp/tabbed-surf.xid)))"
