#!/bin/sh

# Depends on dmenu
pgrep -x dmenu && exit 1

mountable=$(lsblk -lp | grep "part *$" | awk '{print $1, "(" $4 ")"}' && simple-mtpfs --list-devices)
[ "$mountable" = "" ] && exit 1

chosen=$(echo "$mountable" | dmenu -l 20 -i -p "Mount:" | awk '{print $1}')
[ "$chosen" = "" ] && exit 1

case "$chosen" in
    /*)
	udisksctl info -b "$chosen" | awk '/ReadOnly:/{print $2}' | xargs test true = && ( READMODE="ro" && notify-send "(Read-only.)") || READMODE="rw"
	location=$(udisksctl mount -o "$READMODE" -b "$chosen") && pgrep -x dunst && notify-send "$location"

	;;
    *:) simple-mtpfs --device "${chosen%:}" /media/phone && notify-send "Phone mounted."
	;;
esac
#echo ${location%.} | awk '{print $4}' | vifm -


