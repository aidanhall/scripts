#!/bin/bash

[ -z "$1" ] && exit 1

COMPSCRIPT="${2:-1}"

pkill 'entr' && exit 0
echo "$1" | entr -psn "/home/aidan/scripts/compile$COMPSCRIPT.sh $1"
# echo "$1" | entr -sn 'make'
