#!/bin/sh

pgrep -x dmenu && exit

exclusionregex="\(/boot\|/home\|/\)$"

drives=$(lsblk -lp | grep "part */" |grep -v "$exclusionregex" | awk '{print $1, "(" $4 ")", "on", $7}' && echo phone)
[ "$drives" = "" ] && exit 1

chosen=$(echo "$drives" | dmenu -l 20 -i -p "Unmount:" | awk '{print $1}')
[ "$chosen" = "" ] && exit 1

case "$chosen" in
    phone) fusermount -u /media/phone && result="Phone unmounted."
	;;
    /*) result=$(udisksctl unmount -b "$chosen")
	;;
esac

pgrep -x dunst && notify-send "$result"
