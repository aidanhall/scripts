#!/bin/sh

[[ "$1" != "" ]] && cd $1

printf "Name of show: "
read searchterm

wget -O searchfile "https://www.crunchyroll.com/search?o=m&r=t&q=$searchterm"

selectedshow=$(grep clearfix searchfile | grep -v /library/ | grep href | sed 's/ *<a href="//g; s/" class="clearfix">//g; s/\/en-gb\///g' | fzf)
[[ "$selectedshow" = "" ]] && exit 1

wget -O showfile "https://www.crunchyroll.com/$selectedshow"

urls=$(grep episode showfile | grep 'a h' | grep -v js-resume-play | sed 's/<a\ href="//g; s/https:\/\/www.crunchyroll.com//g; s/^ *//g; s/" .*//g; s/\/en-gb//g')

echo "$urls"

title=$(grep "<title>" showfile | grep "Watch on Crunchyroll" | sed 's/ *<title>//g; s/\- Watch on Crunchyroll<\/title>//g')

echo "$title"

mkdir -p "$title"
mv searchfile "$title"
mv showfile "$title"
cd "$title"

rm -f allurlsfile reverseurls
echo "$urls" >> reverseurls
tac reverseurls >> allurlsfile
rm reverseurls
youtube-dl --username aidan.hall@outlook.com --write-sub --sub-lang enUS --restrict-filenames --embed-subs -a allurlsfile
