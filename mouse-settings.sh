#!/bin/sh

# Interactively select a mouse 
DEVICENAME="$(xinput list --name-only | sort | uniq | dmenu -p "Device:" -i -l 20)"
[ -z "$DEVICENAME" ] && exit 1

$HOME/scripts/mousesensible.sh "$DEVICENAME"
