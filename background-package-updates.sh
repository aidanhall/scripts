#!/bin/sh

export UPDATE_STATUS="復"

/usr/bin/notify-send "Downloading update data."
/usr/bin/pacman -Syuw --noconfirm --noprogressbar
/usr/bin/notify-send "Updates downloaded."

export UPDATE_STATUS=""

