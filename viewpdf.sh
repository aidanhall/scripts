#!/bin/sh

pdffiles=$(find $(xdg-user-dir DOCUMENTS) -type f 2>/dev/null | grep '.pdf\|.cbz')
[[ "$pdffiles" = "" ]] && exit 0


selected=$(echo "$pdffiles" | dmenu -i -p "PDF:" -l 10)
[[ "$selected" = "" ]] && exit 0

zathura "$selected"
