#!/bin/sh

MODESELECT="/home/aidan/.screenlayout/$(find /home/aidan/.screenlayout -type f -printf "%P\n" | dmenu -p LAYOUT: -l 20)"

[ -d "$MODESELECT" ] && exit 1

"$MODESELECT"

# Sets wallpaper if the daemon failed.
# pgrep xwallpaper || "$HOME/scripts/random-wallpaper.sh"
(sleep 1s; "$HOME/scripts/random-wallpaper.sh") &
