#!/bin/sh

filename=$(readlink -f "$1")

case "$filename" in
    *\.py) python "$filename"
	;;
    *\.cs) dotnet build
	;;
    *\.html) firefox "$filename"
	;;
    *\.md) pandoc --pdf-engine=pdfroff -V geometry:margin=1cm -o "${filename%\.md}.pdf" "$filename"
	;;
    *\.rs) cargo build
	;;
    *\.ms) grog -k --run -T pdf "$filename" > "${filename%\.ms}.pdf"
	;;
    *\.mm) GROFFOPTS="$(grog -k -Tpdf -mm "$filename")" && mmroff "${GROFFOPTS##groff }" > "${filename%\.mm}.pdf"
	;;
    *\.sh) ./"$filename"
	;;
    *\.mom) pdfmom -epstk "$filename" | ps2pdf - "${filename%\.mom}.pdf"
	;;
    *\.me) groff -k -me "$filename" -T pdf > "${filename%\.me}.pdf"
	;;
    *\.java) javac "$filename"
	;;
    *\.hx) haxe -cp src -main Main --interp && printf "Done!!" && read -r
	;;
    *\.c) gcc -Wall -g -o "${filename%\.c}" "$filename"
	;;
    *\.asm) yasm -g dwarf2 -f elf "$filename" && echo "Assembled." && ld -m elf_i386 -s -o "${filename%\.asm}" "${filename%\.asm}.o" && echo "Linked."
	;;

    *\.cpp) g++ -Wall -std=c++2a -g -o "${filename%\.cpp}" "$filename"
	;;
    *\.pdf) pdftotext "$filename"
	;;
    *\.cob) cobc -x "$filename"
	;;
    *\.pic) setsid pic2graph -density 200 -eqn '$$' < "$filename" > "${filename%\.pic}.png"
	;;
    *\.eqn) setsid eqn2graph -density 200 < "$filename" > "${filename%\.eqn}.png"
	;;
    *\.txt) /home/aidan/programming/plain2md/plain2md < "$filename" | pandoc --pdf-engine=pdfroff -f markdown -o "${filename%\.txt}.pdf"
	;;
    *\.y) bison "$filename"
	;;
    *\.tex) pdflatex "$filename" && biber "${filename%\.tex}" && pdflatex "$filename"
	;;
    *\.mp) mpost "$filename" && mptopdf "${filename%\.mp}.1"
	;;
    *\.bc) bc -wl "$filename"
	;;
    *\.gnu) gnuplot "$filename"
	;;
    *\.lit) groff -U -M "$HOME/programming/literate/roff" -mlit -egGjkspRt -Tpdf < "$filename" > "${filename%\.lit}.pdf"
esac
