#!/bin/sh

playlists=$(find -L $(xdg-user-dir VIDEOS)  -type d 2>/dev/null ; find -L "$(xdg-user-dir DOCUMENTS)/playlists" -type f)
[ -z "$playlists" ]&& exit 1

selected=$(echo "$playlists" | dmenu -l 20 -i -p "Playlist:")
[ -z "$selected" ]&& exit 1

mpv "$selected"
