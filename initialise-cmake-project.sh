#!/bin/sh

[ "$1" = "" ] && echo "Usage: initialise-cmake-repository <foldername> <executablename> <specialtype>" && exit 1

# Set up directories.
projectName="$(readlink -f "$1")"

mkdir -p "$projectName"/src
mkdir -p "$projectName"/build

cd "$projectName" || exit 1

touch src/CMakeLists.txt

printf "\
cmake_minimum_required(VERSION 3.12)
set (project_name \"$2\")
project(\${project_name} VERSION 1.0.0)

" >> src/CMakeLists.txt

# Opencv stuff.
[ "$3" = "opencv" ] && printf "\
find_package( OpenCV REQUIRED )
include_directories( \${OpenCV_INCLUDE_DIRS} )
" >> src/CMakeLists.txt

printf "\
set (CMAKE_CXX_FLAGS \"\${CMAKE_CXX_FLAGS} -Wall -Werror -std=c++17\")

set (source_dir \"\${PROJECT_SOURCE_DIR}\")

file (GLOB source_files \"\${source_dir}/*.cpp\")

add_executable (\${project_name} \"\${source_files}\")
" >> src/CMakeLists.txt

# More Opencv stuff.
[ "$3" = "opencv" ] && printf "\
target_link_libraries( \${project_name} \${OpenCV_LIBS} )
" >> src/CMakeLists.txt

printf "\
cmake -DCMAKE_BUILD_TYPE=Debug -B build -S src
cmake --build build
" > build.sh

printf "\
#include <iostream>

int main()
{
    std::cout<< \"Hello world.\" << std::endl;
}
" > src/main.cpp

chmod +x build.sh
#cmake -DCMAKE_BUILD_TYPE=Debug -S src -B build
