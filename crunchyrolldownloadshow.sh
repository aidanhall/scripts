#!/bin/sh

printf "Name of show: "
read searchterm

wget -O searchfile "https://www.crunchyroll.com/search?from=&q=$searchterm"

selectedshow=$(grep clearfix searchfile | grep -v /library/ | grep href | sed 's/ *<a href="//g; s/" class="clearfix">//g; s/\/en-gb\///g' | fzf)

wget -O showfile "https://www.crunchyroll.com/$selectedshow"

urls=$(grep episode "showfile" | grep 'a h' | grep -v js-resume-play | sed 's/<a\ href="//g; s/https:\/\/www.crunchyroll.com//g; s/^ *//g; s/" .*//g; s/^/https:\/\/www.crunchyroll.com/g')

echo "$urls"

title=$(grep "<title>" $showfile | grep "Watch on Crunchyroll" | sed 's/ *<title>//g; s/\- Watch on Crunchyroll<\/title>//g')

echo "$title"

mkdir -p "$title"
cd "$title"

rm -f urls reverseurls
echo "$urls" >> reverseurls
tac reverseurls >> urls
rm reverseurls

youtube-dl --write-sub --sub-lang enUS --restrict-filenames --embed-subs -a urls

