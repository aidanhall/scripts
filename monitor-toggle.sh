#!/bin/sh

# Toggles between the default monitor layout and just the primary monitor.
# The xwallpaper daemon must be running, or this can run into problems.

(xrandr --listactivemonitors | sed 's/.*:\s*//;q' | xargs test 1 -eq \
    && "$HOME/.screenlayout/default.sh" )\
    || "$HOME/.screenlayout/main.sh"

"$HOME/scripts/random-wallpaper.sh"
