#!/bin/sh

xbacklight -get > /tmp/activebrightness
xbacklight -dec 40 -fps 60 -time 1000
while [ "$(xprintidle)" -ge 1000 ] ; do
    sleep 0.2s
    # if [ "$(xprintidle)" -ge 120000 ] ; then
	# xset dpms force suspend &
	# xargs -a /tmp/activebrightness xbacklight -set
	# break ;
    # fi ;
done
xbacklight -fps 60 -set "$( cat /tmp/activebrightness )"
