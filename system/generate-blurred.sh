#!/bin/sh

pgrep betterlockscreen && exit 1

scrot /tmp/screen.png -o || echo "screenshot failed"

convert /tmp/screen.png -blur 10 -paint 2 -swirl 10 -spread 2 /tmp/blurred.png
betterlockscreen -u /tmp/blurred.png
