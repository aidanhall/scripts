#!/bin/sh

DELIM="  "
PACKAGES=""
HEAVY=""
BATTERY=""
NOTIFYSTATUS=""
VOLUMESTATUS=""
status()
{

    # Playerctl:
    if [ $(($1 % 5)) -eq 1 ] ; then
	playerctl status 2> /dev/null && PLAYERSTATUS=" $(playerctl metadata --format '{{playerName}}: {{emoji(status)}}')$DELIM" || PLAYERSTATUS=""
    fi
    # Packages available.
    if [ $(($1 % 20)) -eq 1 ] ; then
	pacman -Qu | grep -v '\[ignored\]'> /tmp/udcount && PACKAGES="󰏕 $( [ -f /var/lib/pacman/db.lck ] && echo '󰑧' || wc -l < /tmp/udcount )$DELIM" || PACKAGES=""
	# notify-send "$( echo $COUNT | wc -l )"
    fi

    # Volume Icon

    if [ $(($1 % 5)) -eq 1 ] ; then
	AMSTAT="$(amixer sget Master)"
	VOL=$(echo "$AMSTAT" | grep -o -m 1 "\[.*%\]" | tr -d '[]%')
	if ( echo "$AMSTAT" | grep -q "Playback.*\[off\]" ) ; then
	    VOLUMESTATUS="󰖁" 
	else
	    if [ "$VOL" -gt 50 ]; then
		VOLUMESTATUS=" "
	    elif [ "$VOL" -gt 30 ]; then
		VOLUMESTATUS=""
	    else
		VOLUMESTATUS=""
	    fi
	fi
	VOLUMESTATUS="$VOLUMESTATUS $VOL%$DELIM"
    fi

    # RAM

    # Bar indicating usage.
    #echo "^c#000000^^r0,3,40,14^^d^^r1,4,$(free | awk '/^Mem:/ {print $3/$2*38 }'),12^^f40^"

    [ $(($1 % 15)) -eq 0 ] && HEAVY="󰍛 ^c#000000^^r0,3,40,14^^d^^r1,4,$(free | awk '/^Mem:/ {print $3/$2*38 }'),12^^f40^ $DELIM"

    # $(ps axch -o cmd --sort=-%mem | uniq | /home/aidan/scripts/iconize.sed | head -n 5 | tr "\n" ' ')


    # Percentage used.
    #free -h | awk '/^Mem:/ {print $3 "/" $2}'

    # NOTIFICATIONS DISABLED
    [ "$(dunstctl is-paused)" = true ] && NOTIFYSTATUS="$DELIM" || NOTIFYSTATUS=""

    # BATTERY

    # Colourful battery:
    #echo "^r0,3,22,14^^f1^^c#FF0000^^r0,4,5,12^^c#FFAA00^^r5,4,5,12^^c#FFFF00^^r10,4,5,12^^c#00FF00^^r15,4,5,12^^c#000000^^r$(acpi | awk '{ gsub(/,/, ""); print  0.2*$4}'),4,$(acpi | awk '{ gsub(/,/, ""); print 21 - 0.2*$4}'),12^^d^^r21,6,2,8^^f24^"

    # Normal battery:
    if [ "$(hostname)" = "x220" ] && [ $(($1 % 30)) -eq 0 ] ; then
	CHARGELEVEL="$(acpi | grep -v unavailable | awk '{ if (NR > 1) exit; gsub(/,/, ""); print $4 }')"
	CHARGECHECK="$( [ "$( cat /sys/class/power_supply/AC/online )" = 1 ] && printf " 󰚥")"
	BATTERY="^c#bbbbbb^^r0,3,22,14^^f1^^c#000000^^r0,4,20,12^^c#bbbbbb^^r0,4,$(( ${CHARGELEVEL%%%} / 5)),12^^r21,6,2,8^^f24^^d^ $CHARGELEVEL$CHARGECHECK$DELIM"
	fi


    }

PROGRESS=0

while :; do

    STATLINE=""
    #BAR="$(status $PROGRESS | tr '\n' " ")"
    #xsetroot -name "$(status $PROGRESS | tr '\n' " ")"
    status $PROGRESS
    STATLINE=" $NOTIFYSTATUS$PLAYERSTATUS$PACKAGES$STATLINE$VOLUMESTATUS$HEAVY$BATTERY$(date +'%T %d/%m/%y') "

    xsetroot -name "$STATLINE"
    PROGRESS=$(( PROGRESS + 1 ))
    [ $PROGRESS -eq 60 ] && PROGRESS=0
    sleep 1s
done
echo "end"
