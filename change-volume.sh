#!/bin/sh

# Depends on dunst for grouping notifications.

test -z "$@" && exit 1

amixer sset Master "$@" | grep -o -m 1 "\[.*\%\]"| tr -d '[]' | xargs notify-send -u low -c changeVolume -i audio-volume-medium
