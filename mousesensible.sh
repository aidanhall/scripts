#!/bin/sh

# Make the correct configuration choices based on the mouse selected.
[ -z "$1" ] && exit 1

MOUSENAME="$1"

# Default Values

SPEED=1.0
# Note: Acceleration ranges from 1 to -1 with -1 being no acceleration.
ACCEL=-1.0

# Different accelerations and speeds for different mice.
case "$MOUSENAME" in
	("TPPS/2 IBM TrackPoint") ACCEL=-0.25 ;; # TrackPoint technology requires acceleration.
	("Primax Kensington Eagle Trackball") ACCEL=1.0 ; SPEED=0.5 ;; # Low sensitivity for fine-grained motion, acceleration for large moves.
	("SynPS/2 Synaptics TouchPad") ACCEL=1.0;; # Touch pads also require acceleration.
esac

echo "ID: $MOUSENAME"
echo "ACCEL: $ACCEL"
echo "SPEED: $SPEED"

xinput --set-prop "pointer:$MOUSENAME" "Coordinate Transformation Matrix" $SPEED 0 0 0 $SPEED 0 0 0 1
xinput --set-prop "pointer:$MOUSENAME" "libinput Accel Speed" $ACCEL
notify-send "$MOUSENAME" "Acceleration: $ACCEL\nSpeed: $SPEED"
