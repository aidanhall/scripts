#!/bin/sh

# Move to a directory to store shows in.
cd /media/storage/videos || exit

# The search term to be sent to Crunchyroll's database.
printf "Name of show: "
read -r searchterm

# Use Crunchyroll's search engine to find the shows.
wget -O searchfile "https://www.crunchyroll.com/search?from=&q=$searchterm"

# Filter the result to only show the titles of shows and allow the user to select one.
selectedshow=$(grep clearfix searchfile | grep -v /library/ | grep href | sed 's/ *<a href="//g; s/" *class="clearfix">//g; s/\/en-gb\///g' | fzf)
[ "$selectedshow" = "" ] && exit 1

# Download the page for the show.
wget -O showfile "https://www.crunchyroll.com/$selectedshow"

# Filter that down to a list of urls for the episodes, in addition to an 'all' option which can be used to download all of the episodes.
urls="$(grep episode showfile | grep 'a h' | grep -v js-resume-play | sed 's/<a\ href="//g; s/https:\/\/www.crunchyroll.com//g; s/^ *//g; s/" .*//g; s/\/en-gb//g')
all"

echo "$urls"

# Create a folder to put the episodes in with the name of the show.
title=$(grep "<title>" showfile | grep "Watch on Crunchyroll" | sed 's/ *<title>//g; s/ \- Watch on Crunchyroll<\/title>//g')

echo "$title"

# Create a folder with the name of the show to put the video files in.
mkdir -p "$title"
mv searchfile "$title"
mv showfile "$title"
cd "$title" || exit

# Allow the user to select which episodes to download.
chosen=a
rm -f selectedurls
until [ "$chosen" = "" ] ;
do
    chosen=$(echo "$urls" | fzf)
    [ "$chosen" = "all" ] && echo "$urls" | grep -v all | sed 's/^/https:\/\/www.crunchyroll.com\/en-gb/g' >> selectedurls && chosen=""
    urls=$(echo "$urls" | grep -v "$chosen")
    [ "$chosen" = "" ] || echo "$chosen" | sed 's/^/https:\/\/www.crunchyroll.com\/en-gb/g' >> selectedurls ;
done

# Download the episodes.
tac "selectedurls"
tac "selectedurls" | youtube-dl --download-archive downloaded-episodes --write-sub --sub-lang enUS --restrict-filenames --embed-subs --add-metadata --exec "rename 's/^.*Episode/Episode/;s/-.*\\.mp4/\\.mp4/' {}" -a -

# Rename the episodes to make their names shorter.
rename 's/^.*Episode/Episode/;s/-.*\.mp4/\.mp4' "*.mp4"
rm selectedurls showfile searchfile
