#!/bin/sh

printf "Name of show: "
read searchterm

wget -O searchfile "https://www.crunchyroll.com/search?from=&q=$searchterm"

selectedshow=$(grep clearfix searchfile | grep -v /library/ | grep href | sed 's/ *<a href="//g; s/" class="clearfix">//g; s/\/en-gb\///g' | fzf)

wget -O showfile "https://www.crunchyroll.com/$selectedshow"
