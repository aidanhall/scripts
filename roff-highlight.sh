#!/bin/sh

# First, copy the syntax highlighting settings from using pandoc to convert
# a Markdown document into ms into your desired *roff file,
# or set up a file containing them and .so it.

# Usage: roff-highlight <md highlighting class> <filename>

[ -z "$2" ] && exit 1

echo "\`\`\`$1" > /tmp/mdsnippet.md
sed '/^$/a ££££' "$2" >> /tmp/mdsnippet.md
echo "\`\`\`" >> /tmp/mdsnippet.md

pandoc -i /tmp/mdsnippet.md -f markdown -t ms | tail -n +4 | head -n -2 | sed 's/\\\*\[NormalTok \"££££\"\]//'
