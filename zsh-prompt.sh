#!/bin/sh

is_git_repo="$( git rev-parse --is-inside-work-tree 2> /dev/null )"

if [ "$is_git_repo" ]; then
    printf "["
    git branch --show-current | tr '\n' ']'
fi
