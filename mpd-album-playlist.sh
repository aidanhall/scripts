#!/bin/sh

# This script creates a playlist parseable by MPD for the album given as the argument.
# This allows me to easily listen to all the tracks on an album with collabs in order.

cd `xdg-user-dir MUSIC` || exit 1

# [ -z "$1" ] && exit 1 || MUSDIR="$1"
ALBUMS="$*"

# find "$MUSICDIRS" -name "*" -type f -printf "%f\t%h\n" | sort | > $HOME/.config/mpd/playlists/$PLAYLISTFILE.m3u

find . -type d -name "$ALBUMS" | sed 's/^\.\///;' | xargs -d '\n' -I {} find {} -type f -printf "%f\t%h\n" | sort | awk 'BEGIN {FS = "\t" } ; {print $2 "/" $1}'
 # > "$HOME/.config/mpd/playlists/$MUSDIR.m3u"

# awk 'BEGIN {FS = "\t" } ; {print $2 "/" $1}' 
