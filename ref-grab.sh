#!/bin/sh

# Grabs a reference and converts it into refer.

doi=$1

#curl -s "http://api.crossref.org/works/$doi/transform/application/$format" -w "\\n"

# Keyword generator.
rawris=$(curl -s "http://api.crossref.org/works/$doi/transform/application/x-research-info-systems" -w "\\n")

#echo "$rawris"

keyword="$( echo "$rawris" | awk '/AU/{if (author == "") author = tolower(substr($3, 1, length($3)-1)); print author} /PY/{print $3}' | tail -n 2 | tr -d '\n')"

pagerange="$( echo "$rawris" | awk '/SP/{print $3} /EP/{print "-",$3}')"
#echo "$keyword"

#echo "Now for the good stuff:"

refered="%K "$keyword"
%P "$pagerange"
"$(echo "$rawris" | sed '
    /^TY/d;
    s/^DO *- /%K /g;
    s/^TI *- /%T /g;
    s/^UR *- /%O /g;
    s/^PB *- /%I /g;
    s/^AU *- /%A /g;
    s/^A[1-4] *- /%A /g;
    s/^PY *- /%D /g;
    s/^C[1-8] *-/%O /g;
    s/^IS *- /%N /g;
    s/^ED *- /%E /g;
    /^SP/d;
    /^EP/d;
    /^DA/d;
    s/^VL *- /%V /g;
    s/^SN *- /%O /g;
    /^ER/d;
    s/^T2 *- /%O /g;

    
')"
"

echo "$refered"
