#!/bin/sh

# Set the default mouse's sensitivity and applies keyboard configuration (repeat rate and caps<->escape).
# Its behaviour is now broken into multiple scripts, but it calls them to keep its usage the same.

# Possibly my first piece of Linux configuration.

awk '{system("$HOME/scripts/mousesensible.sh \"" $0 "\"")}' "$HOME/scripts/my-mouse"

xinput --set-prop "SynPS/2 Synaptics TouchPad" "libinput Tapping Enabled" 1
xinput --set-prop "SynPS/2 Synaptics TouchPad" "libinput Natural Scrolling Enabled" 1

$HOME/scripts/keyboard-config.sh
