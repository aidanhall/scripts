#!/bin/bash

[ "$1" = "" ] && exit 1

projectName="$(readlink -f "$1")"
targetLanguage="$2"
mkdir -p "$projectName"/src
cd "$projectName" || exit

echo -e "-cp src\n-main Main\n--no-traces" >> build.hxml
echo -e "class Main {\n    static function main() {\n\tSys.println(\"Hello world!\");\n    }\n}" >> src/Main.hx

case "$targetLanguage" in
    java)
	echo -e "-java bin/java" >> build.hxml
	;;
    python)
	echo -e "-python bin/python/out.py" >> build.hxml
	;;
esac
