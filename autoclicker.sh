#!/bin/sh

# An autoclicker for Minecraft farms.

# RATE = 1/attack speed.
RATE="${1:-0.625}"

clickcycle()
{ \
    xdotool click 1
    xdotool mousemove_relative -- 150 0
    sleep "$RATE"
    xdotool click 1
    xdotool mousemove_relative -- -150 0
    sleep "$RATE"
}
# Initial delay before starting.
sleep "${2:-2}"
CYCLES=0
while :; do
    clickcycle
    CYCLES=$((CYCLES+1))
    echo "CYCLE NUMBER: $CYCLES"
done
