#!/bin/sh

# sfeed_update doesn't automatically cancel on slow connections
timeout 20 sfeed_update

# HTML Display, using my slightly modified version of sfeed_html
sfeed_html "$HOME/.sfeed/feeds/"* > "$HOME/.sfeed/feeds.html"

# Run my script to get full descriptions in HTML form.
awk -F '\t' -f "$HOME/.sfeed/wide.awk" "$HOME/.sfeed/feeds/"* > "$HOME/.sfeed/wide.html"
