#!/bin/sh

echo "VIDEO STREAMER"

site="$(printf "YouTube\nCrunchyroll\niPlayer\n" | dmenu -i -p "Website:")"
[ "$site" == "" ] && exit 1
search="$(printf "" | dmenu -i -p "Search:" | sed 's/ /+/g')"
[ "$search" == "" ] && exit 1

# YouTube handling.
if [[ "$site" = YouTube ]] ; then
    # newest version:
    # curl -s "https://www.youtube.com/results?search_query=$search" | grep "yt-uix-tile-link" | sed -e "s/^<\/div><div class=\"yt-lockup-content\"><h3 class=\"yt-lockup-title \"><a href=\"\/watch?v=//; s/^.*;list=/PLAYLIST:/g; s/<\/a><\/div><div class=\"yt-lockup-meta.*//g; s/\" class=\"yt-uix-tile-link.*title=\"/ /g;s/\" .*- Duration: / (/g;s/.<\/span>.* >/) /g;s/\" class=\"yt-lockup-playlist.*dir=\"ltr\">/ /g;s/<\/a>.*//g; s/&#39;/'/g" | grep -v "unsubscribe-label\|<\/span><\/div>"
    videourl="$( curl -s "https://www.youtube.com/results?search_query=$search" | grep "yt-uix-tile-link" | sed -e "s/^<\/div><div class=\"yt-lockup-content\"><h3 class=\"yt-lockup-title \"><a href=\"\/watch?v=//; s/^.*;list=/PLAYLIST:/g; s/<\/a><\/div><div class=\"yt-lockup-meta.*//g; s/\" class=\"yt-uix-tile-link.*title=\"/ /g;s/\" .*- Duration: / (/g;s/.<\/span>.*[a-zA-Z0-9]\" >/) /g;s/\" class=\"yt-lockup-playlist.*dir=\"ltr\">/ /g;s/<\/a>.*//g; s/&#39;/'/g; s/\&amp;/\&/g;" | grep -v "unsubscribe-label\|<\/span><\/div>" | dmenu -i -p "Video:" -l 20 | cut -d " " -f 1 )"
    if [[ "$(echo $videourl | grep "^PLAYLIST")" != "" ]] ; then # If it is a playlist.
	# At the moment, the displayed name for a playlist is that of the second(?) video in the playlist. I would have to make my sed command a lot more comlicated than it already is to change this and you can get the idea of what the playlist probably is based on the second video's name (in reasonable cases).
	videourl="https://www.youtube.com/playlist?list=${videourl##PLAYLIST:}"
	url_type=playlist
    elif [[ "$videourl" == "" ]] ; then # If no URL was selected.
	exit
    else # Normal video URL.
	videourl="https://www.youtube.com/watch?v=$videourl"
    fi
elif [[ "$site" = iPlayer ]] ; then
    echo "iPlayer time!"
    showurl="https://www.bbc.co.uk$(curl -s "https://www.bbc.co.uk/iplayer/search?q=$search" | pup | grep "\/iplayer\/episodes\/" | grep -v "}" | sed 's/^.*href="//g;s/" .*$//g' | dmenu -i -p "Series:" -l 20)"
    showpage="$(curl -s "$showurl" | pup)"
fi
# BBC iPlayer get series URL.
#curl -s "https://www.bbc.co.uk/iplayer/search?q=doctor+who" |pup| grep "\/iplayer\/episodes\/" | grep -v "}" | sed 's/^.*href="//g;s/" .*$//g'

echo "$site"
echo "$videourl"

if [[ "$url_type" == playlist ]] ; then
    videoformat="$(printf "" | dmenu -p "Format:")"
else
    videoformat="$(youtube-dl --list-formats "$videourl" | grep -v 'webpage\|format\|download\|youtube' | dmenu -p "Format:" -l 20 | awk '{print $1 }')"
fi
[ "$videoformat" = "" ] && videoformat="bestvideo+bestaudio"

# Download or stream:
method="$(printf "Stream\nDownload" | dmenu -p "Method:")"

if [[ "$method" == "Stream" ]] ; then
    mpv --ytdl-raw-options=write-auto-sub=,sub-lang=en,format="$videoformat" "$videourl"
else
    notify-send "Downloading..."
    youtube-dl --write-sub --add-metadata --embed-subs --write-auto-sub --write-sub --sub-lang=en --exec "mv {} '$(xdg-user-dir VIDEOS)/$site' && notify-send 'Finished downloading:' {}" --format="$videoformat" "$videourl"
fi
