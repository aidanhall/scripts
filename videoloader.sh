#!/bin/sh

videos=$(find -L $(xdg-user-dir VIDEOS) -type f 2>/dev/null | grep '.mp4\|.webm\|.mkv\|.264\|.flv\|.mpg\|.mpeg\|.avi\|.m4v')
[[ "$videos" = "" ]] && exit 1


selected=$(echo "$videos" | dmenu -i -p "Video:" -l 10)
[[ "$selected" = "" ]] && exit 1

xdg-open "$selected"
