#!/bin/sh

SELECTED=$(find /tmp/mozilla_aidan0 /tmp/qutebrowser-downloads-* "$(xdg-user-dir DOCUMENTS)" \( ! -regex '.*/\([\._]\|node_modules\|eclipse.jdt.ls\|ltximg\).*' \) -type f 2>/dev/null | rofi -matching fuzzy -dmenu -p "FILE")
[ "$SELECTED" = "" ] && exit 0

# xdg-open "$SELECTED"
mimeopen -n "$SELECTED"
