#!/bin/sh

filename=$(readlink -f "$1")

case "$filename" in
    *\.py) python "$filename"
	;;
    *\.cs) dotnet build
	;;
    *\.html) firefox "$filename"
	;;
    *\.md) pandoc -V geometry:margin=1cm -o "${filename%\.md}.pdf" "$filename"
	;;
    *\.rs) cargo build
	;;
    *\.ms) grog --run -T pdf "$filename" > "${filename%\.ms}.pdf"
	;;
    *\.sh) ./"$filename"
	;;
    *\.mom) groff -mom "$filename" -T pdf > "${filename%\.mom}.pdf"
	;;
    *\.me) groff -me "$filename" -T pdf > "${filename%\.me}.pdf"
	;;
    *\.java) javac "$filename"
	;;
    *\.hx) haxe -cp src -main Main --interp && printf "Done!!" && read -r
	;;
    *\.c) gcc -g -o "${filename%\.c}" "$filename"
	;;

    *\.cpp) g++ -g -o "${filename%\.cpp}" "$filename"
	;;
    *\.pdf) pdftotext "$filename"
	;;
esac
