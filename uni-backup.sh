#!/bin/sh

# Back up my files to the university system.
cd "$(xdg-user-dir DOCUMENTS)" || exit 1

rsync -avuz uni u2106099@remote-99.dcs.warwick.ac.uk:~/Documents/rsync
