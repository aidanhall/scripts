#!/bin/sh

SELECTED="$( ( autorandr && printf "common\nclone-largest\nhorizontal\nvertical\noff" ) | cut -d' ' -f1 | dmenu -p LAYOUT: -l 20)"

[ -z "$SELECTED" ] && autorandr -c || autorandr -l "$SELECTED"
