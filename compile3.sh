#!/bin/sh

filename=$(readlink -f $1)

case $filename in
    *\.py) python $filename
	;;
    *\.cs) mono ${filename%\.cs}.exe
	;;
    *\.html) w3m $filename
	;;
    *\.md) pandoc -s "$filename" -o "${filename%\.md}.ms" && groff -ms -t "${filename%\.md}.ms" -T pdf > "${filename%\.md}.pdf"
	;;
    *\.rs) cargo run && sleep 5s
	;;
    #*\.ms) unisub "$filename" | groff -Tpdf -R /home/aidan/Documents/bibliography -ms -e -G -j -s -t - > ${filename%\.ms}.pdf
    *\.ms) printf "Lines\tWords\tCharacters\n" ; pdftotext -layout "${filename%\.ms}.pdf" - | wc
	;;
    *\.mm) GROFFOPTS="$(grog -k -Tpdf -mm "$filename")" && mmroff "${GROFFOPTS##groff }" > "${filename%\.mm}.pdf"
	;;
    *\.mom) GROFFOPTS="$(grog -k -Tpdf -mom "$filename")" && pdfmom "${GROFFOPTS##groff }" | ps2pdf -  "${filename%\.mom}.pdf"
	;;
    *\.cpp) ./build.sh
	;;
    *\.tex) pdflatex "$filename"
	;;
esac

