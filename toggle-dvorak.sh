#!/bin/sh

INDICATOR=$(cat "$HOME/scripts/layouts.txt" | dmenu -p "LAYOUT:" -l 20)

if [ -n "$INDICATOR" ]; then
    setxkbmap $INDICATOR
    notify-send "Mapping: $INDICATOR"
else
    # Default to QWERTY
    setxkbmap -variant "" gb
    notify-send "Restored QWERTY"
fi
