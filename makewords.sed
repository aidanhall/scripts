#!/bin/sed -f

# A sed script that aims to mimic the behaviour of the makewords program
# seen in the AT&T archives film on UNIX.

s/  */\n/g;
s/\t\t*/\n/g;
