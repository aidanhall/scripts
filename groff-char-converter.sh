#!/bin/sh

# A script that outputs the groff character sequence for a unicode character.
#[ $# -ge 1 -a -f "$1" ] && character="$1" || character="-"
character="$1"
man -P cat groff_char 2>/dev/null | grep -- "$character.*\[" | awk '{print $2}'
