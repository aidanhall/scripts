#!/bin/sh

case "$1" in
    *st) tabbed -c st -w ;;
    *zathura) tabbed -c -r 2 zathura -e '' ;;
    *sxiv) tabbed -r 2 -c sxiv -e '' ;;
esac

