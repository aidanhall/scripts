#!/bin/sh

[ "$1" == "" ] && echo "Usage: homestuck-flash-viewer <page number>" && exit

firefox "https://www.homestuck.com/flash/hs2/$1/$1.swf"
