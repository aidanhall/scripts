#!/usr/bin/awk -f

{
    if ($1 == "=>"){
	printf "["
	for ( i = 3; i < NF; i++ ) {
	    printf("%s%s", $i, OFS)
	}
	print $NF "](" $2 ")"
    } else { print }
}
