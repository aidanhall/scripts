#!/bin/sh

# Move to a directory to store shows in.
cd /media/storage/videos || exit

# The search term to be sent to Crunchyroll's database.
printf "Name of show: "
read -r unformattedsearchterm
searchterm="$(echo "$unformattedsearchterm" | sed 's/ /+/g')"

# Filter the result to only show the titles of shows and allow the user to select one.
#selectedshow="$(curl -s "https://www.hidive.com/search?q=$searchterm" | grep -e \/tv\/ -e \/movies\/ | grep -v \<\/a\> | sed -e 's/^.*<a href=\"//;s/\">.*$//' | fzf)"
#[ "$selectedshow" = "" ] && exit 1
selectedshow="$(curl -s "https://www.hidive.com/search?q=$searchterm" | grep -e "\\/tv\\/" -e "\\/movies\\/" | grep -v -e "Show Info" -e "img-rounded" | sed 's/^.*\<a href=\"//;s/\">.*$//' | fzf)"
[ "$selectedshow" = "" ] && exit 1
wget -O showfile "https://www.hidive.com$selectedshow" 
urls="all
$( grep -e "3.5em" showfile | sed 's/^.*<a href=\"//;s/\" style.*$//')"
echo "$urls"
title="$(grep "<title>" showfile | sed 's/^.*<title>Stream //;s/ on HIDIVE<\/title>.*$//')"
echo "$title"
mkdir -p "$title"
mv showfile "$title"
cd "$title" || exit 1

# Episode selection.

chosen=a
rm -f selectedurls
until [ "$chosen" = "" ] ;
do
    chosen="$(echo "$urls" | fzf --tac )"
    [ "$chosen" = "all" ] && echo "$urls" | grep -v all >> selectedurls && chosen=""
    urls="$(echo "$urls" | grep -v "$chosen")"
    [ "$chosen" = "" ] || echo "$chosen"  >> selectedurls ;
done

youtube-dl -u aidan.hall@outlook.com -p "$(cat /home/aidan/scripts/password-hidive)" --download-archive downloaded-episodes --write-sub --all-subs --restrict-filenames --embed-subs --add-metadata --exec "rename 's/.*_s/s/' {}" -a selectedurls

rm selectedurls showfile
