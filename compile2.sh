#!/bin/sh

filename=$(readlink -f $1)

case $filename in
    *\.py) python "$filename"
	;;
    *\.cs) dotnet run
	;;
    *\.html) w3m $filename
	;;
    *\.md) pandoc $filename -V aspectratio:169 -t beamer -o ${filename%\.md}.pdf 
	;;
    *\.rs) cargo run && printf "Done!!!" && read
	;;
    *\.ms) grog -k --run -Tps "$filename" | zathura -
	;;
    *\.mm) grog -k --run -mm -Tps "$filename" | zathura -
	;;
    *\.java) cd "$(dirname "$filename")" && java "$(basename -s .java "$filename")" && sleep 3s
	;;
    *\.hx) haxe build.hxml && printf "Done!!!" && read -r
	;;
    *\.c) "${filename%\.c}"
	;;
    *\.cpp) "${filename%\.cpp}"
	;;
    *\.cob) cobc -x -free "$filename"
	;;
    *\.asm) ${filename%\.asm}
	;;
    *\.pic) sxiv "${filename%\.pic}.png"
	;;
    *\.eqn) sxiv "${filename%\.eqn}.png"
	;;
    *\.y) gcc -lm -o "${filename%\.y}" "${filename%\.y}.tab.c"
	;;
    *\.ml) ocaml "$filename"
	;;
    *\.tex) pdflatex "$filename"
	;;
    *\.bc) bc -wl "$filename"
	;;
    *\.tcl) tclsh "$filename"
	;;
    *\.zig) zig run "$filename"
	;;
esac

