#!/bin/sh

function wttr() {
    # change Paris to your default location
    local request="wttr.in/${1-Cheltenham}"
    [ "$(tput cols)" -lt 125 ] && request+='?1mQ'
    curl -H "Accept-Language: ${LANG%_*}" --compressed "$request"
}

wttr
